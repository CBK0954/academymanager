package com.choibk.academymanager.Interfaces;

public interface CommonModelBuilder<T> {
    T build();
}