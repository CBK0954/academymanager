package com.choibk.academymanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Category {
    PC("컴퓨터", 5),
    MONITOR("모니터", 5),
    MOUSE("마우스", 2)
    ;

    private final String name;
    private final Integer useYear;
}
