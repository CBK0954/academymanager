package com.choibk.academymanager.model;

import com.choibk.academymanager.Interfaces.CommonModelBuilder;
import com.choibk.academymanager.entity.Machine;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED) // setter 대체는 NoAr
public class MachineItem {
    private Long id;
    private String categoryName;
    private String machineName; // 기자재명
    private MachineItem(MachineItemBuilder builder) { //MachineItem의 그릇 생성
        this.id = builder.id;
        this.categoryName = builder.categoryName;
        this.machineName = builder.machineName;
    }

    public static class MachineItemBuilder implements CommonModelBuilder<MachineItem> {
        // CommonModelBuilder 의 MachineItem 을 구현
        private Long id;
        private String categoryName;
        private String machineName;
        public MachineItemBuilder(Machine machine) {
            this.id = machine.getId();
            this.categoryName = machine.getCategory().getName();
            this.machineName = machine.getName();
        }
        @Override
        public MachineItem build() {
            return new MachineItem(this);
        }
    }
}
