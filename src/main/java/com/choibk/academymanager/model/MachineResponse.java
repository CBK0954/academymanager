package com.choibk.academymanager.model;

import com.choibk.academymanager.Interfaces.CommonModelBuilder;
import com.choibk.academymanager.entity.Machine;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED) // 기계가 대신해서 setter 가 필요X
public class MachineResponse {
    private Long id;
    private String name;

    private MachineResponse(MachineResponseBuilder builder) {
        this.id = builder.id;
        this.name = builder.name;
    }

    public static class MachineResponseBuilder implements CommonModelBuilder<MachineResponse> {
        private final Long id;
        private final String name;
        public MachineResponseBuilder(Machine machine) {
            this.id = machine.getId();
            this.name = machine.getName();
        }
        @Override
        public MachineResponse build() {
            return new MachineResponse(this);
        }
    }
}
