package com.choibk.academymanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Setter
@Getter
public class MachineUpdateRequest {
    @NotNull
    @Length(min = 2, max = 10)
    private String name;

}
