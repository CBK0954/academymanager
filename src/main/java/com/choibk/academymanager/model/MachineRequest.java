package com.choibk.academymanager.model;

import com.choibk.academymanager.enums.Category;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class MachineRequest {

    @ApiModelProperty(value = "카테고리", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING )
    private Category category;

    @ApiModelProperty(value = "기자재명 (2 ~ 10자)", required = true)
    @NotNull
    @Length(min = 2, max = 10)
    private String name;

    @ApiModelProperty(value = "장소 (3 ~ 10자)", required = true)
    @NotNull
    @Length(min = 3, max = 10)
    private String place;

    @ApiModelProperty(value = "구입일 (yyyy-mm-dd)", required = true)
    @NotNull
    private LocalDate dateCreate;
}