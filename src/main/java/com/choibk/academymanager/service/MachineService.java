package com.choibk.academymanager.service;

import com.choibk.academymanager.entity.Machine;
import com.choibk.academymanager.exception.CMissingDataException;
import com.choibk.academymanager.model.*;
import com.choibk.academymanager.repository.MachineRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MachineService {
    private final MachineRepository machineRepository;

    public void setMachine(MachineRequest request) {
        Machine addData = new Machine.MachineBuilder(request).build();

        machineRepository.save(addData); //Repository 가 없으면 save 불가능
    }

    public ListResult<MachineItem> getMachines() { // getMachines()이 ListResult<MachineItem> 형태의 모양으로 주기로 약속
        List<Machine> originList = machineRepository.findAll();
        // Repository 에게 모두 가져오라고 명령 후 List<Machine> 타입 originList 모양의 그릇에 담는다
        // 기계가 읽으면 오른쪽부터 읽힘 | 원본 리스트, 전체 리스트

        List<MachineItem> result = new LinkedList<>();
        // new LinkedList<>() 이라는 빈 리스트를 생성 후 List<MachineItem> result 모양의 상자에 담음
        // 재가공 후 결과 값을 담음

        originList.forEach(item -> result.add(new MachineItem.MachineItemBuilder(item).build()));
        // 원본을 하나씩 던지고 결과 값에 하나씩 추가 | result 에 MachineItem 이 들어옴, Builder 를 통해 원본을 주고 build 를 해준다.

        return ListConvertService.settingResult(result);
        // List 를 가공하기 위해서 총 몇 개(페이지)인지 알려주어야함
    }

    public MachineResponse getMachine(long id) {
        // getMachine 을 작동시키기 위해 long 타입을 추가, id는 임시적인 이름
        Machine originData = machineRepository.findById(id).orElseThrow(CMissingDataException::new);
        // 기계는 항상 오른쪽 부터 읽힘 (오른쪽 부터 해석)
        // Repository 라는 창고에 가서 제공 받은 id와 일치하는 데이터 탐색 -> 성공 시 Machine 타입의 originData 박스에 넣는다 (orElseThrow : 없으면 버림)
        // Machine originData = machineRepository.findById(id).orElseThrow(); -> 기본 출구로만 탈출하는 이전에 쓰던 코드

        return new MachineResponse.MachineResponseBuilder(originData).build();
        // builder 를 생성하고 builder 에게 MachineResponse 의 Machine 을 주고 build 를 한 결과를 MachineResponse 에게 준다
    }

    public void putMachine(long id, MachineUpdateRequest request) {
        Machine originData = machineRepository.findById(id).orElseThrow(CMissingDataException::new);
        originData.putMachine(request);

        machineRepository.save(originData);
    }

    public void delMachine(long id) {
        machineRepository.deleteById(id);
    }
}