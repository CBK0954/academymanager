package com.choibk.academymanager.entity;

import com.choibk.academymanager.Interfaces.CommonModelBuilder;
import com.choibk.academymanager.enums.Category;
import com.choibk.academymanager.model.MachineRequest;
import com.choibk.academymanager.model.MachineUpdateRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Machine {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 15)
    private Category category;

    @Column(nullable = false, length = 10)
    private String name;

    @Column(nullable = false, length = 10)
    private String place;

    @Column(nullable = false)
    private LocalDate dateCreate;

    public void putMachine(MachineUpdateRequest request) {
        this.name = request.getName();
    }

    private Machine(MachineBuilder builder) {
        this.category = builder.category;
        this.name = builder.name;
        this.place = builder.place;
        this.dateCreate = builder.dateCreate;
    }

    public static class MachineBuilder implements CommonModelBuilder<Machine> {

        private final Category category;
        private final String name;
        private final String place;
        private final LocalDate dateCreate;

        public MachineBuilder(MachineRequest request) {
            this.category = request.getCategory();
            this.name = request.getName();
            this.place = request.getPlace();
            this.dateCreate = request.getDateCreate();
        }
        @Override
        public Machine build() {
            return new Machine(this);
        }
    }
}
