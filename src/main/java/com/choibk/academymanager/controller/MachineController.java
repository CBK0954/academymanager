package com.choibk.academymanager.controller;

import com.choibk.academymanager.model.*;
import com.choibk.academymanager.service.MachineService;
import com.choibk.academymanager.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "학원 기자재 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/machine") // swagger 사용
public class MachineController {
    private final MachineService machineService;

    @ApiOperation(value = "기자재 등록")
    @PostMapping("/data")
    public CommonResult setMachine(@RequestBody @Valid MachineRequest request) {
        machineService.setMachine(request);

        return ResponseService.getSuccessResult(); // 성공,실패 여부 출력 (오류 메시지가 친절해짐)
    }

    @ApiOperation(value = "기자재 전체 조회")
    @GetMapping("/all")
    public ListResult<MachineItem> getMachines() {
        // 원래는 List<MachineItem> -> 기존
        return ResponseService.getListResult(machineService.getMachines(), true);
        // return machineService.getMachines(); -> 기존
        // isSuccess 내용이 빈 상태로 출력
    }

    @ApiOperation(value = "기자재 id 조회")
    @GetMapping("/detail/{id}") // {id}값을 받아오기 위해 @PathVariable
    public SingleResult<MachineResponse> getMachine(@PathVariable long id) {
    //public MachineResponse getMachine(@PathVariable long id) { // 이전 수업 형태
        return ResponseService.getSingleResult(machineService.getMachine(id));
        //return machineService.getMachine(id); // 이전 수업 형태
    }

    @ApiOperation(value = "기자재 수정")
    @PutMapping("/rename/{id}")
    public CommonResult putMachine(@PathVariable long id, @RequestBody @Valid MachineUpdateRequest request) {
        //@ long id를 @RB 쪽으로 바꿔라
        machineService.putMachine(id, request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "기자재 삭제")
    @DeleteMapping("/{id}")
    public CommonResult delMachine(@PathVariable long id) {
        machineService.delMachine(id);

        return ResponseService.getSuccessResult();
    }
}
